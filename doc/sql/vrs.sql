/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.199.40
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : 192.168.199.40:3306
 Source Schema         : vrs

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 02/05/2023 03:27:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_user`;
CREATE TABLE `t_admin_user`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_admin_user
-- ----------------------------
INSERT INTO `t_admin_user` VALUES (1, '2023-05-01 00:24:34', '2023-05-01 00:24:36', 'admin', 'admin');

-- ----------------------------
-- Table structure for t_car
-- ----------------------------
DROP TABLE IF EXISTS `t_car`;
CREATE TABLE `t_car`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `admin_user_id` bigint(0) NOT NULL,
  `brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `door_num` int(0) NULL DEFAULT NULL,
  `fuel_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `package_num` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `price` decimal(38, 2) NULL DEFAULT NULL,
  `rental` decimal(38, 2) NOT NULL,
  `seat_num` int(0) NULL DEFAULT NULL,
  `speed_changer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int(0) NOT NULL,
  `type` int(0) NULL DEFAULT NULL,
  `consumption` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `car_type` int(0) NULL DEFAULT NULL,
  `car_shop_id` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK8op8drqhq2bqp5b7tj5ivg6sa`(`car_shop_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_car
-- ----------------------------
INSERT INTO `t_car` VALUES (1, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马', 4, '95#汽油', 'https://img1.baidu.com/it/u=4181002221,761244478&fm=253&fmt=auto&app=120&f=JPEG?w=889&h=500', 'x5', '5', 650000.00, 500.00, 5, '自动档', 0, 4, '1.5L', 2, 1);
INSERT INTO `t_car` VALUES (2, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (3, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (4, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (5, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (6, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (7, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (8, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (9, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (10, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (11, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);
INSERT INTO `t_car` VALUES (12, '2023-05-01 01:31:28', '2023-05-01 01:31:30', 1, '宝马测试', 4, '95#汽油', 'https://youjia-image.cdn.bcebos.com/modelImage/4b46e905e3e619adaebfcdf607662da5/16599285144597266694.jpg@!1200_width', 'x1', '5', 250000.00, 200.00, 5, '自动档', 0, 1, '1.5L', 1, 1);

-- ----------------------------
-- Table structure for t_car_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_car_shop`;
CREATE TABLE `t_car_shop`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `down_work_time` datetime(0) NOT NULL,
  `goto_wrok_time` datetime(0) NOT NULL,
  `repay_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `shop_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `tel` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_car_shop
-- ----------------------------
INSERT INTO `t_car_shop` VALUES (1, '2023-05-01 22:13:19', '2023-05-01 22:13:21', '虹桥上海宝马4s店', '2023-05-01 09:00:00', '2023-05-01 18:00:00', '虹桥上海宝马4s店左侧门', '虹桥宝马4s店', '16544523445');

-- ----------------------------
-- Table structure for t_date
-- ----------------------------
DROP TABLE IF EXISTS `t_date`;
CREATE TABLE `t_date`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `available` bit(1) NOT NULL,
  `date` date NOT NULL,
  `rental_date` date NOT NULL,
  `return_date` date NOT NULL,
  `car_id` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKt262e08imcgg7nfyxileh2wcm`(`car_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of t_date
-- ----------------------------
INSERT INTO `t_date` VALUES (1, b'1', '2023-05-01', '2023-05-01', '2023-05-25', 1);

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `day` int(0) NOT NULL,
  `end_date` date NOT NULL,
  `finish_time` datetime(0) NULL DEFAULT NULL,
  `start_date` date NOT NULL,
  `status` int(0) NOT NULL,
  `total_price` decimal(38, 2) NOT NULL,
  `car_id` bigint(0) NULL DEFAULT NULL,
  `user_id` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKn5jo15rs5c5ucoob4ovscxyrb`(`car_id`) USING BTREE,
  INDEX `FKho2r4qgj3txpy8964fnla95ub`(`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES (1, '2023-05-01 17:27:05', '2023-05-01 17:27:07', 20, '2023-05-21', '2023-05-21 17:27:26', '2023-05-21', 1, 10000.00, 1, 1);
INSERT INTO `t_order` VALUES (2, '2023-05-01 19:37:22', '2023-05-01 19:37:23', 12, '2023-05-13', '2023-05-13 19:37:20', '2023-05-01', 1, 6000.00, 1, 2);
INSERT INTO `t_order` VALUES (3, '2023-05-01 21:32:51', '2023-05-01 21:32:51', 20, '2023-05-21', '2023-05-21 21:32:51', '2023-05-01', 1, 10000.00, 1, 3);
INSERT INTO `t_order` VALUES (4, '2023-05-01 21:36:40', '2023-05-01 21:36:40', 12, '2023-05-13', '2023-05-13 21:36:40', '2023-05-01', 1, 2400.00, 2, 3);
INSERT INTO `t_order` VALUES (5, '2023-05-01 21:37:48', '2023-05-01 21:37:48', 12, '2023-05-13', '2023-05-13 21:37:48', '2023-05-01', 1, 6000.00, 1, 3);
INSERT INTO `t_order` VALUES (6, '2023-05-02 00:56:49', '2023-05-02 00:56:49', 12, '2023-05-14', '2023-05-14 00:56:49', '2023-05-02', 1, 6000.00, 1, 1);
INSERT INTO `t_order` VALUES (7, '2023-05-02 01:16:05', '2023-05-02 01:16:05', 12, '2023-05-14', '2023-05-14 01:16:05', '2023-05-02', 1, 6000.00, 1, 1);
INSERT INTO `t_order` VALUES (8, '2023-05-02 03:22:11', '2023-05-02 03:22:11', 30, '2023-06-01', '2023-06-01 03:22:11', '2023-05-02', 1, 6000.00, 3, 4);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` int(0) NOT NULL,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_8919pbk2o3mjy7h7h6tolmk6d`(`phonenumber`) USING BTREE,
  UNIQUE INDEX `UK_jhib4legehrm4yscx9t3lirqi`(`username`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, '2023-05-01 11:23:31', '2023-05-01 11:23:31', '小花', '$2a$10$0FaYl/in7a7OB8vQaNBDuOZ7s3x3e6lJR17XZogzhpPqauLGdajEO', '16522522553', 1, '111', '20230501/1682928964227.png');
INSERT INTO `t_user` VALUES (2, '2023-05-01 16:16:12', '2023-05-01 16:16:12', '小草', '$2a$10$IMsD/iV3s38raX/2GpgZPeonjiusHbgdM5lD6HDsOzoxrl1677ram', '16788933674', 1, '222', '20230501/1682928964227.png');
INSERT INTO `t_user` VALUES (3, '2023-05-01 21:31:43', '2023-05-01 21:31:43', 'eee小花', '$2a$10$bjRastQ2gdmzLloQxrXbUO.ro3w.S.lDZGUernfiUBbL/Zeo8Ke02', '15988988978', 1, 'eee', '20230501/1682947900436.png');
INSERT INTO `t_user` VALUES (4, '2023-05-02 03:18:04', '2023-05-02 03:18:04', NULL, '$2a$10$2Qr6FVqTw2Jh6/OhJWQVuelqBMQiphTJZ4lU9A1ILVHYfn7FTv8Mi', '17655422334', 1, 'zzz', '20230502/1682968682187.png');

SET FOREIGN_KEY_CHECKS = 1;
