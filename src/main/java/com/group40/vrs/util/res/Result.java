package com.group40.vrs.util.res;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * ajax提交统一返回结果类
 * @author Administrator
 *
 */
public class Result<T> {
	
	private int code;//错误码
	
	private String msg;//返回的具体信息
	
	private T data;//定义返回的数据
	
	private Result(){}//构造函数私有化,不允许任意创建对象

	/**
	 * 定义传codemsg的私有化构造函数，不允许外部创建对象
	 * @param codeMsg
	 */
	private Result(CodeMsg codeMsg){
		if(codeMsg != null){
			this.code = codeMsg.getCode();
			this.msg = codeMsg.getMsg();
		}
	}

	private Result(int code, String codeMsg){
		this.code = code;
		this.msg = codeMsg;
	}
	
	/**
	 * 定义传指定数据对象和codemsg的私有化构造函数，不允许任意创建对象
	 * @param data
	 * @param codeMsg
	 * @return
	 */
	private Result(T data,CodeMsg codeMsg){
		if(codeMsg != null){
			this.code = codeMsg.getCode();
			this.msg = codeMsg.getMsg();
		}
		this.data = data;
	} 
	
	/**
	 * 定义统一的成功返回函数, 一般用这个就可以了
	 * @param data
	 * @return
	 */
	public static <T>Result<T> success(T data){
		return new Result<T>(data,CodeMsg.SUCCESS);
	}

	/**
	 * 统一错误返回方法，所有错误都调用此方法
	 * @param codeMsg
	 * @return
	 */
	public static <T>Result<T> error(CodeMsg codeMsg){
		return new Result<T>(codeMsg);
	}

	public static <T>Result<T> error(int code, String codeMsg){
		return new Result<T>(code, codeMsg);
	}

	/**
	 * 可以修改 200 状态的返回
	 * @param data
	 * @return
	 * @param <T>
	 */
	public static <T>ResponseEntity<Result<T>> successOk(T data){
		return ResponseEntity.<Result<T>>ok(new Result<T>(data,CodeMsg.SUCCESS));
	}

	/**
	 * 可以修改 401 状态的返回
	 * @return
	 * @param <T>
	 */
	public static <T>ResponseEntity<Result<T>> unauthorized(){
		return ResponseEntity.<Result<T>>status(HttpStatus.UNAUTHORIZED).body(new Result<T>(null, CodeMsg.ADMIN_NO_RIGHT));
	}


	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	
}
