package com.group40.vrs.util.session;
/**
 * 关于session的所有常量统一存放类
 * @author Administrator
 *
 */
public class SessionConstant {

	public static final String SESSION_USER_LOGIN_KEY = "user_user";

	public static final String SESSION_ADMIN_LOGIN_KEY = "admin_admin";
}
