package com.group40.vrs.service;

import java.time.LocalDate;
import java.util.List;

import com.group40.vrs.entity.Date;

public interface DateService {
    
    List<Date> findCarsBetween(LocalDate from, LocalDate to);
    Object create();
}
