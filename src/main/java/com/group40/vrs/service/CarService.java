package com.group40.vrs.service;

import com.group40.vrs.entity.Car;
import com.group40.vrs.entity.CarRequestParam;
import com.group40.vrs.util.res.PageBean;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CarService {

    List<Car> getAllCars();

    Car getCarById(Long id);

    Car addCar(Car car);

    Car updateCar(Long id, Car car);

    void deleteCar(Long id);

    void rentCar(Long id);

    void returnCar(Long id);

    //查询可用车辆
    List<Car> findAvailableCars(LocalDate startDate, LocalDate endDate);

    //按ID查询可用车辆
    Optional<Car> findAvailableCarById(Long id, LocalDate startDate, LocalDate endDate);

    //搜索可用车辆
    List<Car> searchAvailableCars(String model, String brand, LocalDate startDate, LocalDate endDate);

    //更新车辆可用性
    //具体来说，方法中的传入参数为一个车辆对象和一个可用性布尔值，遍历，将它们的 available 属性设置为，并将这些日期更新到 DateRepository 中
    //如果available被设置为true，则表示该车在指定时间段内可以出租；如果被设置为false,则表示该车在该时间段内不可用。
    void updateCarAvailability(Car car, boolean available);

    public PageBean<Car> findlist(PageBean<Car> pageBean, CarRequestParam carRequestParam);
}
