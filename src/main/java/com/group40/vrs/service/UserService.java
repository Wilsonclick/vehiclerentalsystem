package com.group40.vrs.service;

import com.group40.vrs.entity.User;

public interface UserService {

    User save(User user);

    User findByUsername(String username);
}
