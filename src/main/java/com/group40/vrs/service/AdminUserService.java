package com.group40.vrs.service;

import com.group40.vrs.entity.AdminUser;

import java.util.List;

public interface AdminUserService {
    AdminUser newAdminUser(AdminUser adminUser);

    List<AdminUser> getAdmin();

    List<AdminUser> clearAdminList();

    List<AdminUser> deleteOneAdmin(AdminUser adminUser);

    AdminUser checkAdminUser(String name, String password);
    
}
