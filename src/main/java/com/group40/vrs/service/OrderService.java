package com.group40.vrs.service;


import java.time.LocalDate;
import java.util.List;
//import java.util.Optional;

import com.group40.vrs.entity.AddOrderParams;
import com.group40.vrs.entity.Date;
import com.group40.vrs.entity.Order;
import com.group40.vrs.util.res.PageBean;

public interface OrderService {
    List<Order> getAllOrders();

    Order findOrder(long orderId);

    Order updateOrder(long orderId, Integer status);

    Order save(Order order);

    Order rentCar(Long carId, LocalDate startDate, LocalDate endDate);

    List<Order> findOrdersByCarId(Long carId);

    boolean cancelOrder(Long orderId);

    List<Order> queryOrderByUserId(Long userId);

    void addOrder(AddOrderParams addOrderParams);

    PageBean<Order> findlist(PageBean<Order> pageBean);
}



/* 
import java.util.List;
import com.cpt202.models.Order;
import com.cpt202.repostories.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    @Autowired
    private OrderRepo orderRepository;

    
    public List<Order> findAll() {
        return orderRepository.findAllOrder();
    }

    public Order findOrder(int orderId) {
        return orderRepository.findOrder(orderId);
    }

    
    public void updateOrder(int orderId, String status) {
        Order order = orderRepository.findOrder(orderId);
        if (order != null) {
            order.setStatus(status);
            orderRepository.save(order);
        }
    }

    public void save(Order order) {
        orderRepository.save(order);
    }
}
*/