package com.group40.vrs.service.impl;

import com.group40.vrs.entity.User;
import com.group40.vrs.repository.UserRepository;
import com.group40.vrs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public User save(User user) {
        user.setStatus(1);
        User save = userRepository.save(user);
        return save;
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findUsersByUsername(username);
    }
}
