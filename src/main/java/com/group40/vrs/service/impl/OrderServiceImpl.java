package com.group40.vrs.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
//import java.util.Optional;

import com.group40.vrs.entity.*;
import com.group40.vrs.entity.Order;
import com.group40.vrs.repository.CarRepository;
import com.group40.vrs.repository.DateRepository;
import com.group40.vrs.repository.OrderRepository;
import com.group40.vrs.service.CarService;
import com.group40.vrs.service.UserService;
import com.group40.vrs.util.DateUtils;
import com.group40.vrs.util.res.PageBean;
import jakarta.persistence.criteria.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.group40.vrs.repository.OrderRepo;
import com.group40.vrs.service.OrderService;


@Service
public class OrderServiceImpl implements OrderService {

    
    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private DateRepository dateRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<Order> getAllOrders() {
        return orderRepo.findAll();
    }

    @Override
    public Order findOrder(long orderId) {
        return orderRepo.findById(orderId).get();
    }

    @Override
    public Order updateOrder(long orderId, Integer status) {
        Order order = orderRepo.findById(orderId).get();
        if (order != null) {
            order.setStatus(status);
            orderRepo.save(order);
        }
        return order;
    }

    @Override
    public Order save(Order order) {
        return orderRepo.save(order);
    }



    @Autowired
    private CarService carService;

    @Override
    public Order rentCar(Long carId, LocalDate startDate, LocalDate endDate) {
        Car car = carRepository.findById(carId).orElse(null);
        if (car == null) {
            return null;
        }
        List<Date> dates = dateRepository.findByCarAndAvailableTrueAndDateBetween(car, startDate, endDate);
        if (dates.isEmpty()) {
            return null;
        }
        double totalPrice = car.getPrice().doubleValue() * (endDate.toEpochDay() - startDate.toEpochDay());
        Order order = new Order();
        order.setCar(car);
        order.setStartDate(startDate);
        order.setEndDate(endDate);
        order.setTotalPrice(BigDecimal.valueOf(totalPrice));
        orderRepository.save(order);
        carService.updateCarAvailability(car, false);
        // 不能从CarService类型中对非静态方法updateCarAvailability(Car, boolean)进行静态引用
        // 加了 static
        return order;
    }

    @Override
    public List<Order> findOrdersByCarId(Long carId) {
        Car car = carRepository.findById(carId).orElse(null);
        if (car == null) {
            return null;
        }
        return orderRepository.findByCarId(carId);
    }

    @Override
    public boolean cancelOrder(Long orderId) {
        Order order = orderRepository.findById(orderId).orElse(null);
        if (order == null) {
            return false;
        }
        carService.updateCarAvailability(order.getCar(), true);
        orderRepository.delete(order);
        return true;
    }

    @Override
    public List<Order> queryOrderByUserId(Long userId) {
        return orderRepository.findByUserId(userId);
    }

    @Override
    public void addOrder(AddOrderParams addOrderParams) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principalTemp = authentication.getPrincipal();
        if (principalTemp instanceof org.springframework.security.core.userdetails.User) {
            org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) principalTemp;
            // 拿出登录人的用户名
            String username = principal.getUsername();
            if (null != username && username.length() > 0) {
                // 查询用户信息
                User byUsername = userService.findByUsername(username);
                // 查询汽车信息
                Car car = carRepository.findById(addOrderParams.getCarId()).get();
                Order order = new Order();
                order.setFinishTime(DateUtils.addDay(new java.util.Date(), addOrderParams.getDay()));
                order.setCar(car);
                order.setUser(byUsername);
                order.setDay(addOrderParams.getDay());
                order.setStatus(1);
                order.setCreateTime(new java.util.Date());
                order.setUpdateTime(new java.util.Date());
                LocalDate localDate = new java.util.Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                order.setStartDate(localDate);

                java.util.Date date = DateUtils.addDay(new java.util.Date(), addOrderParams.getDay());
                LocalDate localDate1 = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                order.setEndDate(localDate1);
                order.setTotalPrice(car.getRental().multiply(new BigDecimal(addOrderParams.getDay())));
                orderRepository.save(order);
            }
        }
        throw new RuntimeException("创建订单失败");
    }

    /**
     * 分页查询订单
     * @param pageBean
     * @return
     */
    @Override
    public PageBean<Order> findlist(PageBean<Order> pageBean) {
        Specification<Order> specification = new Specification<Order>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Order> root,
                                         CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                Predicate[] p = new Predicate[list.size()];
                return criteriaBuilder.and(list.toArray(p));
            }
        };
        // 按照租金排序
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");
        PageRequest pageable = PageRequest.of(pageBean.getCurrentPage()-1, pageBean.getPageSize(), sort);
        Page<Order> findAll = orderRepository.findAll(specification, pageable);
        pageBean.setContent(findAll.getContent());
        pageBean.setTotal(findAll.getTotalElements());
        pageBean.setTotalPage(findAll.getTotalPages());
        return pageBean;
    }


}



