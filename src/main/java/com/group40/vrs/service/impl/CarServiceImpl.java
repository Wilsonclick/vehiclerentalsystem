package com.group40.vrs.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.group40.vrs.entity.CarRequestParam;
import com.group40.vrs.entity.Date;
import com.group40.vrs.repository.DateRepository;
import com.group40.vrs.util.res.PageBean;
import jakarta.persistence.criteria.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.group40.vrs.entity.Car;
// import com.project.vrs.entity.Order;
import com.group40.vrs.repository.CarRepository;
import com.group40.vrs.service.CarService;

@Service
public class CarServiceImpl implements CarService{

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private static DateRepository dateRepository;
    //自动注入一个名为D.Repo的对象，用于访问数据库中的Date表

    @Override
    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    @Override
    public Car getCarById(Long id) {
        Optional<Car> optionalCar = carRepository.findById(id);
        return optionalCar.orElse(null);
    }

    @Override
    public Car addCar(Car car) {
        return carRepository.save(car);
    }

    @Override
    public Car updateCar(Long id, Car car) {
        Car existingCar = getCarById(id);
        if (existingCar != null) {
            existingCar.setBrand(car.getBrand());
            existingCar.setModel(car.getModel());
            existingCar.setPrice(car.getPrice());
            carRepository.save(existingCar);
        }
        return existingCar;
    }

    @Override
    public void deleteCar(Long id) {
        carRepository.deleteById(id);
    }

    @Override
    public void rentCar(Long id) {
        Car car = getCarById(id);
    }

    // public void returnCar(Long id) {
    //     Car car = getCarById(id);
    //     if (car != null && car.isRented()) {
    //         car.setRented(false);
    //         carRepository.save(car);
    //     }
    // }
//     public void returnCar(Long id) {
//         Car car = carRepository.findById(id).orElse(null);
//         if (car != null && car.getIsRented()) {
//             car.setIsRented(false);
//             carRepository.save(car);

//             List<Order> orders = orderService.getAllOrdersByCarId(id);
//             for (Order order : orders) {
//                 if (!"completed".equals(order.getStatus())) {
//                     order.setStatus("completed");
//                     orderService.updateOrder(order);
//                 }
//             }
//         }
//     }
//     // CarService.returnCar(id);
// }

    @Override
    public void returnCar(Long id) {
    }

    //查询可用车辆
    @Override
    public List<Car> findAvailableCars(LocalDate startDate, LocalDate endDate) {
        List<Car> availableCars = carRepository.findAll(); //从CarRepo中查询到所有的车辆
        for (Car car : availableCars) {
            List<Date> dates = dateRepository.findByCarAndAvailableTrueAndDateBetween(car, startDate, endDate); //查找该车辆在指定时间内是否被预订过
            if (!dates.isEmpty()) {
                car.setDates(dates); //如果车辆被预订，则将这些日期添加到该车辆的属性dates中
            } else {
                availableCars.remove(car); //如果没有被预订，则在availableCars中移除该车辆
            }
        }
        return availableCars; //finally返回可用车辆列表
    }

    //按ID查询可用车辆
    @Override
    public Optional<Car> findAvailableCarById(Long id, LocalDate startDate, LocalDate endDate) {
        Optional<Car> optionalCar = carRepository.findById(id); //通过ID参数从CarRepo中查询到指定车辆
        if (optionalCar.isPresent()) {
            Car car = optionalCar.get();
            List<Date> dates = dateRepository.findByCarAndAvailableTrueAndDateBetween(car, startDate, endDate); //查找该车辆在指定时间内是否被预订过
            if (!dates.isEmpty()) {
                car.setDates(dates); //如果车辆被预订，则将这些日期添加到该车辆的属性dates中，并返回optionalCar
            } else {
                optionalCar = Optional.empty(); //如果没有被预订，则返回Optional.empty()
            }
        }
        return optionalCar;
    }

    @Override
    //搜索可用车辆
    public List<Car> searchAvailableCars(String model, String brand, LocalDate startDate, LocalDate endDate) {
        List<Car> availableCars = carRepository.findAllByModelContainingIgnoreCaseAndBrandContainingIgnoreCase(model, brand); //通过model和brand参数从CarRepo中查询到与这些参数相关的车辆
        for (Car car : availableCars) {
            List<Date> dates = dateRepository.findByCarAndAvailableTrueAndDateBetween(car, startDate, endDate);
            if (!dates.isEmpty()) {
                car.setDates(dates); //如果车辆被预订，则将这些日期添加到该车辆的属性dates中
            } else {
                availableCars.remove(car); //如果没有被预订，则在availableCars中移除该车辆
            }
        }
        return availableCars; //最终返回可用车辆列表
    }

    //更新车辆可用性
    //具体来说，方法中的传入参数为一个车辆对象和一个可用性布尔值，遍历，将它们的 available 属性设置为，并将这些日期更新到 DateRepository 中
    @Override
    public void updateCarAvailability(Car car, boolean available) { //接收两个参数：一个类型为Car的对象car,和一个类型为boolean的变量available
        for (Date date : car.getDates()) { //for循环遍历了该车辆的所有日期
            date.setAvailable(available); //对于每一个日期，都将其可用性设置为传入的布尔值available
            dateRepository.save(date); //并通过dateRepo.save方法将该日期的变更保存到数据库中
        } //这个方法的作用是更新指定车辆在指定时间段内的可用性状态
    }     //如果available被设置为true，则表示该车在指定时间段内可以出租；如果被设置为false,则表示该车在该时间段内不可用。

    /**
     * 分页查询
     * @param pageBean
     * @param carRequestParam
     * @return
     */
    @Override
    public PageBean<Car> findlist(PageBean<Car> pageBean, CarRequestParam carRequestParam) {

        Specification<Car> specification = new Specification<Car>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Car> root,
                                         CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                // 类型比对
                if (null != carRequestParam.getType()) {
                    Path<String> type = root.get("type");
                    Predicate wfNameLike = criteriaBuilder.equal(type, carRequestParam.getType());
                    list.add(criteriaBuilder.and(wfNameLike));

                }
                // 单日租金
                if (null != carRequestParam.getMinp() && null != carRequestParam.getMaxp()) {
                    Path<BigDecimal> rental = root.get("rental");
                    Predicate wfNameLike = criteriaBuilder.between(rental, BigDecimal.valueOf(carRequestParam.getMinp()), BigDecimal.valueOf(carRequestParam.getMaxp()));
                    list.add(criteriaBuilder.and(wfNameLike));
                }
                Predicate[] p = new Predicate[list.size()];
                return criteriaBuilder.and(list.toArray(p));
            }
        };
        // 按照租金排序
        Sort sort = Sort.by(Sort.Direction.DESC, "rental");
        PageRequest pageable = PageRequest.of(pageBean.getCurrentPage()-1, pageBean.getPageSize(), sort);
        Page<Car> findAll = carRepository.findAll(specification, pageable);
        pageBean.setContent(findAll.getContent());
        pageBean.setTotal(findAll.getTotalElements());
        pageBean.setTotalPage(findAll.getTotalPages());
        return pageBean;
    }
}
