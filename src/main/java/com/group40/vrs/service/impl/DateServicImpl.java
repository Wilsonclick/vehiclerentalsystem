package com.group40.vrs.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.group40.vrs.entity.Date;
import com.group40.vrs.repository.DateRepository;
import com.group40.vrs.service.DateService;


@Service
public class DateServicImpl implements DateService {
    @Autowired
	private static DateRepository availRepository;

	

	

	/**
	 * @param from
	 * @param to
	 * @return
	 */
	@Override
	public  List<Date> findCarsBetween(LocalDate from, LocalDate to) {
		
        return availRepository.findCarByDate(from, to);
	}




    @Override
    public Object create() {
        return null;
    }
  
}
