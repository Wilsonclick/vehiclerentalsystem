package com.group40.vrs.service.impl;

import java.util.List;

import com.group40.vrs.service.AdminUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.group40.vrs.entity.*;
import com.group40.vrs.repository.*;

@Service
public class AdminUserServiceImpl implements AdminUserService {
    @Autowired
    private AdminUserRepo adminUserRepo;

    @Override
    public AdminUser newAdminUser(AdminUser adminUser){
       return adminUserRepo.save(adminUser);
    }

    @Override
    public List<AdminUser> getAdmin(){
        return adminUserRepo.findAll();
    }

    @Override
    public List<AdminUser> clearAdminList(){
        adminUserRepo.deleteAll();
        return adminUserRepo.findAll();
    }

    @Override
    public List<AdminUser> deleteOneAdmin(AdminUser adminUser){
        adminUserRepo.delete(adminUser);
        return adminUserRepo.findAll();
    }

    @Override
    public AdminUser checkAdminUser(String name, String password) {
        AdminUser adminUser=adminUserRepo.findByUsernameAndPassword(name, password);
        return adminUser;
    }

}
