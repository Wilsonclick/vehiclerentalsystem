package com.group40.vrs.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.group40.vrs.entity.Date;
import com.group40.vrs.service.impl.DateServicImpl;

import java.util.List;


@RestController
//@RequestMapping()
public class DateController {
    
    @Autowired
    DateServicImpl dateServicImpl;
    

    @GetMapping("car-date/{from}/{to}")
    public List<Date> findDate(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to) {
        return dateServicImpl.findCarsBetween(from, to);
    }
}
