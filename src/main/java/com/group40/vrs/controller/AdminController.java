package com.group40.vrs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.group40.vrs.entity.AdminUser;
import com.group40.vrs.service.impl.AdminUserServiceImpl;

import org.springframework.ui.Model;



//注解为restcontroller，提供给前端调用
//admincontroller:admin的界面，包括查看列表、编辑订单状态
@RestController
@RequestMapping("/root")
public class AdminController{
  


@Autowired
private AdminUserServiceImpl adminUserServiceiImpl;

//localhost:8080/maintainAdmin
    @GetMapping("/list")
    public String getAdminList(Model model){
        model.addAttribute("adminList",adminUserServiceiImpl.getAdmin());
        return "allAdmin";
    }

    @GetMapping("/add")
    public String addAdmin(Model model){
      model.addAttribute("admin", new AdminUser());
      return "addAdmin";
    }

    @PostMapping("/add")
    public String confirmNewAdmin(@ModelAttribute("admin")AdminUser adminUser){
      //add the new admin into database
      adminUserServiceiImpl.newAdminUser(adminUser);
      return"root";
    }

    @PostMapping("/adminLogin")
    public String adminLogin(@RequestParam("username") String username,
                             @RequestParam("password") String password){
        //add the new admin into database
        System.out.println("这里能接收到数据吗");
        return"root";
    }


    

}
    