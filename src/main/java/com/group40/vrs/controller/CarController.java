package com.group40.vrs.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.group40.vrs.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.group40.vrs.entity.Car;

// import jakarta.servlet.http.HttpServletRequest;
// import jakarta.servlet.http.HttpSession;

@RestController
@RequestMapping("/cars")
public class CarController {


    @Autowired
    private CarService carService;
    
    @GetMapping("/{id}")
    public ResponseEntity<Car> getCarById(@PathVariable Long id) {
        Car car = carService.getCarById(id);
        return ResponseEntity.ok(car);
    }

    @GetMapping("getAllCars")
    public ResponseEntity<List<Car>> getAllCars() {
        List<Car> cars = carService.getAllCars();
        return ResponseEntity.ok(cars);
    }

    @PostMapping
    public ResponseEntity<Car> addCar(@RequestBody Car car) {
        Car newCar = carService.addCar(car);
        return ResponseEntity.status(HttpStatus.CREATED).body(newCar);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Car> updateCar(@PathVariable Long id, @RequestBody Car car) {
        Car updatedCar = carService.updateCar(id, car);
        return ResponseEntity.ok(updatedCar);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCar(@PathVariable Long id) {
        carService.deleteCar(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{id}/rent")
    public ResponseEntity<Void> rentCar(@PathVariable Long id) {
        carService.rentCar(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{id}/return")
    public ResponseEntity<Void> returnCar(@PathVariable Long id) {
        carService.returnCar(id);
        return ResponseEntity.noContent().build();
    }




    // 调用carService的findAvailableCars方法
    // 获取在指定时间内可用的所有的汽车列表
    @GetMapping
    public List<Car> getAvailableCars(
            @RequestParam(value = "start_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(value = "end_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        List<Car> cars = carService.findAvailableCars(startDate, endDate);
        return cars;
    }

    // 调用carService的findAvailableCarById方法
    // 查找指定id的汽车是否在给定时间段内可用
    // 如果存在，返回OptionalCar entity
    @GetMapping("/two/{id}")
    public Optional<Car> getAvailableCarById(
            @PathVariable(value = "id") Long carId,
            @RequestParam(value = "start_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(value = "end_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        Optional<Car> optionalCar = carService.findAvailableCarById(carId, startDate, endDate);
        return optionalCar;
    }

    // 调用carService的searchAvailableCars方法
    // 返回在指定时间内可用的所有满足特定品牌或型号的汽车列表
    @GetMapping("/search")
    public List<Car> searchAvailableCars(
            @RequestParam(value = "model", required = false) String model,
            @RequestParam(value = "brand", required = false) String brand,
            @RequestParam(value = "start_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(value = "end_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        List<Car> cars = carService.searchAvailableCars(model, brand, startDate, endDate);
        return cars;
    }

    // 调用carService的findAvailableCarById方法
    // 查找指定id的汽车是否在给定时间段内可用
    // 如果存在，使用carService的updateCarAvailability方法更新汽车的可用情况
    @PutMapping("/two/{id}")
    public void updateCarAvailability(
            @PathVariable(value = "id") Long carId,
            @RequestParam(value = "available") boolean available) {
        Optional<Car> optionalCar = carService.findAvailableCarById(carId, LocalDate.now(), LocalDate.MAX);
        if (optionalCar.isPresent()) {
            Car car = optionalCar.get();
            carService.updateCarAvailability(car, available);
        }
    }
}
