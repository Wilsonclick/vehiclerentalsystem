package com.group40.vrs.controller;

import com.group40.vrs.util.res.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import com.group40.vrs.entity.User;
import com.group40.vrs.service.UserService;

import java.util.Date;

@RestController
@RequestMapping("/user")
public class UserLoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/register")
    @ResponseBody
    public Result<User> registerUser(@RequestBody User user) {
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        return Result.success(userService.save(user));
    }

//    @GetMapping("/{username}")
//    public User getUserByUsername(@PathVariable String username) {
//        return userService.findByUsername(username);
//    }
}

