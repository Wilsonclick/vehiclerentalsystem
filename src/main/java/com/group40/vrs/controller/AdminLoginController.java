package com.group40.vrs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.group40.vrs.entity.AdminUser;
import com.group40.vrs.service.impl.AdminUserServiceImpl;

import jakarta.servlet.http.HttpSession;

@Controller
@RequestMapping("/admintemp")
public class AdminLoginController {
    
    @Autowired
    private AdminUserServiceImpl adminUserServiceImpl;
    


    @GetMapping
    public String adminLoginPage(){
        return "admin/login";
    }


    @PostMapping("/login")
    public String adminLogin(@RequestParam String name,@RequestParam String password,HttpSession session,RedirectAttributes attributes){
        AdminUser adminUser=adminUserServiceImpl.checkAdminUser(name, password);
        if(adminUser!=null){
            adminUser.setPassword(null);
            session.setAttribute("adminUser", adminUser);
            return"admin/index";
        }else{
            attributes.addFlashAttribute("message", "Invalid adminUser name &/ password!");
            return"redirect:/admin";
        }
    }

    @GetMapping("/index")
    public String adminLogOut(HttpSession session){
        session.removeAttribute("adminUser");
        return"redirect:/admin";
    }
}
