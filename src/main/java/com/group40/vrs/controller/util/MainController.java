package com.group40.vrs.controller.util;

import com.group40.vrs.entity.*;
import com.group40.vrs.service.*;
import com.group40.vrs.util.res.PageBean;
import com.group40.vrs.util.session.SessionConstant;
import com.group40.vrs.util.session.SessionUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 统一调度所有的html页面
 * 就是为了统一管理，不然有的走 static下访问，有的 走 template 下访问，太乱套了 规定所有的页面放到 template 下， js css img 等静态的资源放到static下
 *
 */
@Controller
public class MainController {

    @Autowired
    private CarService carService;

    @Autowired
    private DateService dateService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private AdminUserService adminUserService;


    @GetMapping("/")
    public String home(Model model){
        return "redirect:/userpage/carlist";
    }

    @RequestMapping("/adminpage/login")
    public String gotoAdminLogin() {
        return "/admin/login/login";
    }

    /**
     * 登录失常，跳转主页
     * @return
     */
    @RequestMapping("/admin/login")
    public String gotoAdminMain(HttpServletRequest request, AdminUser adminUser) {
        //检查一切符合，可以登录，将用户信息存放至session
        AdminUser adminUser1 = adminUserService.checkAdminUser(adminUser.getUsername(), adminUser.getPassword());
        request.getSession().setAttribute(SessionConstant.SESSION_ADMIN_LOGIN_KEY, adminUser1);
        return "redirect:/adminpage/orderlist";
    }

    @RequestMapping("/adminpage/orderlist")
    public String gotoAdminPage(PageBean<Order> pageBean, Model model) {
        PageBean<Order> findlist = orderService.findlist(pageBean);
        model.addAttribute("pageBean", findlist);
        return "/admin/order/orderlist";
    }

    /**
     * admin 退出接口
     * @return
     */
    @RequestMapping(value="/admin/logout")
    public String logout(){
        AdminUser loginedUser = SessionUtil.getLoginedUser();
        if(loginedUser != null){
            SessionUtil.set(SessionConstant.SESSION_ADMIN_LOGIN_KEY, null);
        }
        return "redirect:/adminpage/orderlist";
    }

    @RequestMapping("/adminpage/cardetail")
    public String gotoAdminCarDetail(Long carId, Model model) {
        Car carById = carService.getCarById(carId);
        model.addAttribute("carInfo", carById);
        return "/admin/car/cardetail";
    }

    @GetMapping("/adminpage/orderdetail")
    public String gotoAdminOrderdetail(Model model, Long orderId) {
        // 直接获取当前登陆人的信息
        Order order = orderService.findOrder(orderId);

        if (null == order) {
            order = new Order();
        }
        model.addAttribute("orderInfo", order);
        return "/admin/order/orderdetail";
    }



    /**
     * 密码登录的时候可能市场，直接跳转到主页
     * @return
     */
    @RequestMapping("/user/login")
    public String gotoUserMain() {
        return "redirect:/userpage/carlist";
    }

    /**
     * 去user 的登录注册页面
     * @return
     */
    @RequestMapping("/userpage/login")
    public String gotoUserLogin() {
        return "/user/login/login";
    }

    /**
     * 去 car的列表页面
     * @return
     */
    @GetMapping("/userpage/carlist")
    public String gotoCarList(PageBean<Car> pageBean, Model model, CarRequestParam carRequestParam) {
        Integer type = carRequestParam.getType();
        if (null != type) {
            model.addAttribute("type", type);
        }
        Integer maxp = carRequestParam.getMaxp();
        Integer minp = carRequestParam.getMinp();
        if (null != maxp && null != minp) {
            model.addAttribute("maxp", maxp);
            model.addAttribute("minp", minp);
        }

        model.addAttribute("pageBean", carService.findlist(pageBean, carRequestParam));
        return "/user/car/carlist";
    }

    /**
     * 去 car的详情页面
     * @return
     */
    @RequestMapping("/userpage/cardetail")
    public String gotoCarDetail(Long carId, Model model) {
        Car carById = carService.getCarById(carId);
        model.addAttribute("carInfo", carById);
        return "/user/car/cardetail";
    }

    /**
     * 去 订单页面
     * @return
     */
    @GetMapping("/userpage/orderdetail")
    public String gotoOrderdetail(Model model) {
        // 直接获取当前登陆人的信息
        Order order = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principalTemp = authentication.getPrincipal();
        if (principalTemp instanceof org.springframework.security.core.userdetails.User) {
            org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User)principalTemp;
            // 拿出登录人的用户名
            String username = principal.getUsername();
            if (null != username && username.length() > 0) {
                // 查询用户信息
                User byUsername = userService.findByUsername(username);
                List<Order> orders = orderService.queryOrderByUserId(byUsername.getId());
                if (!CollectionUtils.isEmpty(orders)) {
                    order = orders.get(0);
                }
            }
        }
        if (null == order) {
            order = new Order();
        }
        model.addAttribute("orderInfo", order);
        return "/user/order/orderdetail";
    }
}