package com.group40.vrs.controller;


import com.group40.vrs.entity.AddOrderParams;
import com.group40.vrs.entity.Car;
import com.group40.vrs.service.OrderService;
import com.group40.vrs.util.res.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.group40.vrs.entity.Order;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/viewOrder")
    public String viewOrder(Model model, @RequestParam("orderId") int orderId) {
        Order order = orderService.findOrder(orderId);
        if (order == null) {                                          //null
            model.addAttribute("message", "Order does not exist!");
        } else {
            model.addAttribute("order", order);
        }
        return "viewOrder";
    }

    @PostMapping("/changeOrderStatus")
    public String changeOrderStatus(@RequestParam("orderId") int orderId,
                                    @RequestParam("status") String status, Model model) {

        if ("waiting".equals(status)) {                                //waitingh
            orderService.updateOrder(orderId, 1);
            model.addAttribute("message", "Arranged car!");

        } else if ("done".equals(status)) {
            model.addAttribute("message", "Completed processing!");    //done

        } else {                                                       //非
            model.addAttribute("message", "Error!");
        }
        return "viewOrder";
    }

    // 将该方法映射到一个POST类型的HTTP请求上，请求路径为"/{car_id}"
    // 调用OrderService中的rentCar()方法
    // 传入车辆ID、起始日期和截止日期作为参数，将返回的订单保存在order变量中，将刚创建的订单返回
    @PostMapping("/{car_id}") //要租赁的车辆id
    public Order rentCar(
            @PathVariable(value = "car_id") Long carId,
            @RequestParam(value = "start_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(value = "end_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        Order order = orderService.rentCar(carId, startDate, endDate);
        return order;
    }

    // 调用OrderService中的findOrdersByCarId()方法
    //传入车辆ID作为参数，将返回的订单列表保存在orders变量中，将查询到的订单列表返回。
    @GetMapping("/{car_id}") //要查询订单的车辆id
    public List<Order> getOrdersByCarId(
            @PathVariable(value = "car_id") Long carId) {
        List<Order> orders = orderService.findOrdersByCarId(carId);
        return orders;
    }

    // 调用OrderService的cancelOrder()方法
    // 传入要取消的订单ID作为参数，在数据库中将此订单标记为已取消，并返回操作结果
    @DeleteMapping("/{order_id}") //要取消的订单id
    public boolean cancelOrder(
            @PathVariable(value = "order_id") Long orderId) {
        boolean result = orderService.cancelOrder(orderId);
        return result;
    }

    @GetMapping("/user/{user_id}") //要取消的订单id
    public ResponseEntity<List<Order>> queryOrderByUserId(
            @PathVariable(value = "user_id") Long userId) {
        List<Order> result = orderService.queryOrderByUserId(userId);
        return ResponseEntity.ok(result);
    }


    @PostMapping("addOrder")
    public Result<Void> addOrder(@RequestBody AddOrderParams addOrderParams) {
        try {
            orderService.addOrder(addOrderParams);
            return Result.<Void>success(null);
        } catch (Exception e) {
            return Result.<Void>error(null);
        }

    }
}
