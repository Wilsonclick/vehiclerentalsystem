package com.group40.vrs.repository;

import java.time.LocalDate;
import java.util.List;

import com.group40.vrs.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.group40.vrs.entity.Date;


public interface DateRepository extends JpaRepository<Date,Integer>, JpaSpecificationExecutor<Date> {
    
    @Query("SELECT c FROM Date c WHERE c.rentalDate BETWEEN :from AND :to ")
	List<Date> findCarByDate(@Param("from") LocalDate from, @Param("to") LocalDate to);

    List<Date> findByCarAndAvailableTrueAndDateBetween(Car car, LocalDate startDate, LocalDate endDate);
}
