package com.group40.vrs.repository;

// import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.group40.vrs.entity.Order;

 @Repository
public interface OrderRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {
    List<Order> findByCarId(Long carId);

     List<Order> findByUserId(Long userId);
 }

//一个基于Spring Data JPA的订单数据访问接口，使用了@Repository注解来表明它是一个数据访问组件。
//该接口继承了 JpaRepository 接口，因此它具有了一些基本的CRUD方法，如save()，delete()等。
//此外，该接口还定义了一个自定义的方法findByCarId(Long carId)，用于根据给定的汽车ID，查找与之相关的所有订单。
//这个方法的实现是由Spring Data JPA自动完成的，只需按照约定命名即可，不需要手动实现。
//最终执行的SQL语句是类似于 SELECT * FROM orders WHERE car_id = ? 的形式。