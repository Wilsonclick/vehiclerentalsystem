package com.group40.vrs.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.group40.vrs.entity.Order;

@Repository
public interface OrderRepo extends JpaRepository <Order, Long> {
    //List<Order> findByOrderId(long orderId);
    //List<com.group40.vrs.entity.Order> findAll();

    //Order saveOrder(Order order);
}
