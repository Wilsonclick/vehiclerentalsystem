package com.group40.vrs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.stereotype.Repository;

import com.group40.vrs.entity.Car;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

// @Repository
public interface CarRepository extends JpaRepository<Car, Long> , JpaSpecificationExecutor<Car> {

    List<Car> findAllByModelContainingIgnoreCaseAndBrandContainingIgnoreCase(String model, String brand);
}
