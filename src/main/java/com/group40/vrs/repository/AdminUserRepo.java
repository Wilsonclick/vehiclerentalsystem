package com.group40.vrs.repository;
import com.group40.vrs.entity.*;

import org.springframework.data.jpa.repository.JpaRepository;

//<>内部第一个写要让spring创建的对象，第二个写表的主键的数据类型
public interface AdminUserRepo extends JpaRepository<AdminUser,Long>{
    //通过@query注解来手写sql语句
    //@Query(value = "select * from admin_user", nativeQuery = true)
    //List<AdminUser> getAll();
    //@Query(value = "select * from admin_user where name=", nativeQuery = true)
    AdminUser findByUsernameAndPassword(String username, String password);
}
