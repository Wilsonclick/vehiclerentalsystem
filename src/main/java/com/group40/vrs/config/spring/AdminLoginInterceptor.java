package com.group40.vrs.config.spring;

import com.alibaba.fastjson.JSON;
import com.group40.vrs.util.StringUtil;
import com.group40.vrs.util.res.CodeMsg;
import com.group40.vrs.util.session.SessionConstant;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.io.IOException;

/**
 * 管理访问控制
 */
@Component
public class AdminLoginInterceptor implements HandlerInterceptor {

    private Logger log = LoggerFactory.getLogger(AdminLoginInterceptor.class);
    @Override
    public boolean  preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        String requestURI = request.getRequestURI();
        HttpSession session = request.getSession();
        Object attribute = session.getAttribute(SessionConstant.SESSION_ADMIN_LOGIN_KEY);
        if(attribute == null){
            log.info("用户还未登录或者session失效,重定向到登录页面,当前URL=" + requestURI);
            //首先判断是否是ajax请求
            if(StringUtil.isAjax(request)){
                //表示是ajax请求
                try {
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(JSON.toJSONString(CodeMsg.USER_SESSION_EXPIRED));
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return false;
            }
            //说明是普通的请求，可直接重定向到登录页面
            //用户还未登录或者session失效,重定向到登录页面
            try {
                response.sendRedirect("/adminpage/login");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return false;
        }
        log.info("该请求符合登录要求，放行" + requestURI);
        return true;
    }
}
