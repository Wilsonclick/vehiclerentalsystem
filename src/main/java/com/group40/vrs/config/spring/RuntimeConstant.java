package com.group40.vrs.config.spring;

import java.util.Arrays;
import java.util.List;

/**
 * 系统运行时的常量
 * @author Administrator
 *
 */
public class RuntimeConstant {

	//后台登录拦截器无需拦截的url
	public static List<String> loginExcludePathPatterns = Arrays.asList(
			// 放行所有关于user的
			"/userpage/**",
			"/user/**",

			// 放行静态资源
			"/static/**",
			"/css/**",
			"/img/**",
			"/js/**",
			"/layui/**",

			// 放行接口 上传和请求图片接口
			"/photo/**",
			"/upload/**",
			// 放行下订单接口
			"/orders/addOrder",

			// 放行 admin 接口和页面
			"/admin/login",
			"/adminpage/login"
		);

}
