package com.group40.vrs.config.security;
import com.group40.vrs.util.HttpServletUtil;
import com.group40.vrs.util.res.Result;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;



/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-03-21 16:48
 */
@Slf4j
public class CustomAuthenticationFailureHandler extends
        SimpleUrlAuthenticationFailureHandler {

    public CustomAuthenticationFailureHandler(){
        super("/userpage/login?error");
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        AuthenticationException e) throws IOException, ServletException {
        if (e instanceof LockedException) {
            e = new LockedException("账户被锁定，请联系管理员!", e);
        } else if (e instanceof CredentialsExpiredException) {
            e = new CredentialsExpiredException("密码过期，请联系管理员!", e);
        } else if (e instanceof AccountExpiredException) {
            e = new AccountExpiredException("账户过期，请联系管理员!", e);
        } else if (e instanceof DisabledException) {
            e = new DisabledException("账户被禁用，请联系管理员!", e);
        } else if (e instanceof BadCredentialsException) {
            e = new BadCredentialsException("账号密码输入错误，请重新输入!", e);
        } else if (e instanceof UsernameNotFoundException) {
            e = new BadCredentialsException("用户不存在，请重新输入!", e);
        }
        MediaType acceptMediaType = HttpServletUtil.getRequestPrimaryAcceptMediaType(httpServletRequest);
        if(MediaType.TEXT_HTML.compareTo(acceptMediaType)==0){
            super.onAuthenticationFailure(httpServletRequest, httpServletResponse, e);
        }else if(MediaType.APPLICATION_JSON.compareTo(acceptMediaType)==0 || MediaType.ALL.compareTo(acceptMediaType)==0){
            Result<String> error = Result.<String>error(400, "登陆失败" + e.getMessage());
            httpServletResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
            httpServletResponse.setContentType("text/plain;charset=utf-8");
            httpServletResponse.getWriter().write(error.toString());
        }
    }
}