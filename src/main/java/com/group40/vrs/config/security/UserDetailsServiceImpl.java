package com.group40.vrs.config.security;


import com.group40.vrs.entity.AdminUser;
import com.group40.vrs.entity.User;
import com.group40.vrs.repository.UserRepository;
import com.group40.vrs.util.res.CodeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * 手动实现一个UserDetailsService
 * @return
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        /**
         * 从数据库查:User是我自己写的类,getOne()是mybatis_plus的中的方法
         * 就通过用户名查出一个用户
         */
        // 这个要从数据库取出来
        User usersByUsername = userRepository.findUsersByUsername(username);
        if (null == usersByUsername) {
            throw new UsernameNotFoundException(CodeMsg.ADMIN_USERNAME_NO_EXIST.getMsg());
        }
        /**
         * 创建一个权限集合,随你放多少
         * 最后返回一个org.springframework.security.core.userdetails包下的User
         * 授权就给完了
         * 我这里授权的是USER;
         */
        ArrayList<SimpleGrantedAuthority> arrayList = new ArrayList<>();
        arrayList.add(new SimpleGrantedAuthority("ROLE_USER"));
        return new org.springframework.security.core.userdetails.User(usersByUsername.getUsername(),usersByUsername.getPassword(),arrayList);
    }
}
