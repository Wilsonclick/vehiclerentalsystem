package com.group40.vrs.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@Configuration
// 启动类增加EnableMethodSecurity注解
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
public class SpringSecurityConfig {

    @Bean
    public SimpleUrlAuthenticationFailureHandler getSimpleUrlAuthenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }
    /**
     * 配置一个实现加密的 bean
     * spring security 帮忙实现好了直接注入
     * @return
     */
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        // 不加密的
//        return NoOpPasswordEncoder.getInstance();
        return new BCryptPasswordEncoder();
    }


    /**
     * 配置用户的登录
     * 如果要添加一个用户的权限控制，不要让管理员的影响到
     * @param http
     * @return
     * @throws Exception
     */
    @Bean
    public SecurityFilterChain userSecurityFilterChain(HttpSecurity http) throws Exception {

        // 不需要权限管理的页面
        String noLimitUrlArr[] = new String[]{
                // 放行页面
                "/userpage/login",

                // 放行静态资源
                "/static/**",
                "/css/**",
                "/img/**",
                "/js/**",
                "/layui/**",

                // 放行接口
                "/user/register",
                "/photo/**",
                "/upload/**",

                // 放行 admin 接口和页面
                "/admin**",
                "/admin/**",
                "/adminpage/**",
        };

        return http.authorizeHttpRequests(authorize-> {
                    try {
                        authorize
                                // 放行
                                .requestMatchers(noLimitUrlArr).permitAll()

                                // 其余的都需要权限校验
                                .anyRequest().authenticated()

                                 // 记住密码  传参 on 即可
                                .and().rememberMe().rememberMeParameter("remeber_me")
                                .and()
                                /**
                                 * 1. 表单的提交方式
                                 * 自定义登陆页面是loginSelf.html页(这里看你模板引擎怎么配),表单提交的处理链接是"/upLogin"
                                 * 就是<form action="/upLogin"></form>这样,这个"/upLogin"不需要自己处理,就自己决定
                                 * usernameParameter和passwordParameter,就是表单提交所带的参数,
                                 * 这里我是"username"和"password";
                                 *
                                 * 2. ajax 方式请求，这种方式就放行就行了
                                 */
                                /**
                                 * .loginPage这个是定义跳转登录页面的url
                                 * .loginProcessingUrl代表的是定义的form表单提交的url
                                 * 上面着俩出现了，就是用页面 from 登录的方式
                                 */
                                // 注意:配置了loginPage不用loginProcessingUrl,默认的提交链接就是loginPage的参数,如果值配置loginProcessingUrl那么登陆页面就是默认滴!!!
                                .formLogin()
                                // 配置管理员的
                                .loginPage("/userpage/login")
                                .usernameParameter("username").passwordParameter("password")
                                .loginProcessingUrl("/user/login")
                                .defaultSuccessUrl("/userpage/carlist")
                                .failureUrl("/userpage/login?error")
                                .failureHandler(getSimpleUrlAuthenticationFailureHandler())
                                .and().logout().logoutUrl("/userpage/logout").logoutSuccessUrl("/user/login")

                                .and()
                                // 防跨站请求伪造
                                .csrf(csrf -> csrf.disable());



                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
        ).build();
    }


    /**
     * @Configuration
     * @EnableWebSecurity
     * public class WebSecurityConfig
     * {
     *     @Autowired
     *     MyDBAuthenticationService myDBAuthenticationService;
     *     @Autowired
     *     public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception
     *     {
     *         auth.userDetailsService(myDBAuthenticationService);
     *     }
     *     @Configuration
     *     @Order(1)
     *     public static class WebConfigurationAdapter1 extends WebSecurityConfigurerAdapter
     *     {
     *         @Override
     *         protected void configure(HttpSecurity http) throws Exception
     *         {
     *             http
     *                     //.authorizeRequests().antMatchers("/am/**").access("hasRole('ROLE_AM')")
     *                     .antMatcher("/am/**").authorizeRequests().anyRequest().hasRole("AM")
     *                     .and()
     *                     .exceptionHandling()
     *                     .accessDeniedPage("/403")
     *                     .and()
     *                     .formLogin()
     *                     .loginPage("/amLogin")
     *                     .loginProcessingUrl("/am/postLogin")
     *                     .defaultSuccessUrl("/am/chatPage")
     *                     .failureUrl("/amLogin?error")
     *                     .and().logout().logoutUrl("/am/logout").logoutSuccessUrl("/amLogoutSuccessful")
     *                     .deleteCookies("JSESSIONID")
     *                     .and().csrf().disable();
     *             System.out.println("1st Configurer");
     *         }
     *     }
     *     @Configuration
     *     @Order(2)
     *     public static class WebConfigurationAdapter2 extends WebSecurityConfigurerAdapter
     *     {
     *         @Override
     *         protected void configure(HttpSecurity http) throws Exception
     *         {
     *             http
     *                     //.authorizeRequests().antMatchers("/customer/**").access("hasRole('ROLE_CUSTOMER')")
     *             .antMatcher("/admin/**").authorizeRequests().anyRequest().hasRole("CUSTOMER")
     *                     .and()
     *                     .exceptionHandling()
     *                     .accessDeniedPage("/403")
     *                     .and()
     *                     .formLogin()
     *                     .loginPage("/customerLogin")
     *                     .loginProcessingUrl("/customer/postLogin")
     *                     .defaultSuccessUrl("/customer/chatPage")
     *                     .failureUrl("/customerLogin?error")
     *                     .and().logout().logoutUrl("/customer/logout").logoutSuccessUrl("/customerLogoutSuccessful")
     *                     .and().csrf().disable();
     *             System.out.println("2nd Configurer");
     *         }
     *     }
     * }
     */
}
