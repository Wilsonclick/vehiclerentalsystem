package com.group40.vrs.entity;

import lombok.Data;

@Data
public class CarRequestParam implements java.io.Serializable{
    private static final long serialVersionUID = 1L;

    private Integer type;

    private Integer minp;

    private Integer maxp;

    private String brand;

    private Date startTime;

    private Date endTime;
}
