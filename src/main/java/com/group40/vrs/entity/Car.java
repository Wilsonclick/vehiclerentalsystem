package com.group40.vrs.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 车辆信息
 */
@Data
@Entity
@Table(name = "t_car")
public class Car extends BaseEntity {

    // 管理员id
    @Column(nullable = false, name = "admin_user_id")
    private Long adminUserId;

    // 车辆品牌，如宝马 奔驰
    @Column(nullable = false, name = "brand")
    private String brand;

    // 车辆的型号 如 x5 e-class
    @Column(nullable = false, name = "model")
    private String model;

    // 车辆价格
    @Column(nullable = true, name = "price")
    private BigDecimal price;

    // 车辆单日租金
    @Column(nullable = false, name = "rental")
    private BigDecimal rental;

    // 状态 0 未出租， 1 已出租  3 表示维修中
    @Column(nullable = false, name = "status")
    private Integer status;

    // 门数
    @Column(nullable = true, name = "door_num")
    private Integer doorNum;

    // 座椅数
    @Column(nullable = true, name = "seat_num")
    private Integer seatNum;

    // 行李容量
    @Column(nullable = true, name = "package_num")
    private String packageNum;

    // 燃料类型
    @Column(nullable = true, name = "fuel_type")
    private String fuelType;

    // 变速器
    @Column(nullable = true, name = "speed_changer")
    private String speedChanger;

    // 排量
    @Column(nullable = true, name = "consumption")
    private String consumption;

    // 型号 2.suv 3.mpv  1.普通轿车 4.A级车 5.B级车 6.C级车 7.加长版
    @Column(nullable = true, name = "car_type")
    private int carType;

    // 展示图
    @Column(nullable = true, name = "image")
    private String image;

    // 型号 1. 经济  2. 舒适  3. 商务 4.豪华
    @Column(nullable = true, name = "type")
    private Integer type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_shop_id")
    private CarShop carShop;

    //关联日期表
    //关联日期表
    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Date> dates;


}