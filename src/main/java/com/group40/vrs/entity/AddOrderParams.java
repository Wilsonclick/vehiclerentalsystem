package com.group40.vrs.entity;

import lombok.Data;

@Data
public class AddOrderParams implements java.io.Serializable{
    private static final long serialVersionUID = 1L;

    private Integer day;

    private Long carId;

    private Long userId;

}
