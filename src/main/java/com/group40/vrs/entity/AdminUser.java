package com.group40.vrs.entity;

import jakarta.persistence.*;
import lombok.Data;


/**
 * 管理员，不重要
 */
@Data
//注解为实体类型，会为他创建表 管理员实体类
@Entity
@Table(name = "t_admin_user")
public class AdminUser extends BaseEntity {

    @Column(nullable = false, name = "username")
    private String username;

    @Column(nullable = false, name = "password")
    private String password;

    
}
