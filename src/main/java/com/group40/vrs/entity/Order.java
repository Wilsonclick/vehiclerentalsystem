package com.group40.vrs.entity;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import jakarta.persistence.*;
import lombok.Data;

/**
 * 订单实体类
 */
@Data
@Entity
@Table(name = "t_order")
public class Order extends BaseEntity {

    @Temporal(TemporalType.TIMESTAMP)
    private Date finishTime;

    // 汽车信息
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id")
    private Car car;

    // 用户信息
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate endDate;

    // 总的租金
    @Column(nullable = false)
    private BigDecimal totalPrice;

    // 租了多少天
    @Column(nullable = false)
    private Integer day;

    // 1 有效 2 失效
    @Column(nullable = false)
    private Integer status;


}