package com.group40.vrs.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="t_user")
public class User extends BaseEntity {

    // 登录名 唯一性
    @Column(length = 100, nullable = false, name = "username", unique=true)
    private String username;

    // 用户昵称
    @Column(length = 100, nullable = true, name = "nick_name")
    private String nickName;

    // 登录密码
    @Column( nullable = false, name = "password")
    private String password;

    // 手机号 唯一性
    @Column(length = 11, nullable = false, unique=true)
    private  String phonenumber;

    // 证件图片字段
    @Column(nullable = false, unique=true)
    private  String image;

    //0有效 1无效
    @Column(nullable = false, name = "status")
    private Integer status;


}
