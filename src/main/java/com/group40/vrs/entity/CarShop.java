package com.group40.vrs.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table(name = "t_car_shop")
public class CarShop extends BaseEntity {

    // 租车商店名称
    @Column(nullable = false, name = "shop_name")
    private String shopName;

    // 车店上班时间
    @Column(nullable = false, name = "goto_wrok_time")
    private Date gotoWrokTime;

    // 车店下班时间
    @Column(nullable = false, name = "down_work_time")
    private Date downWorkTime;

    // 详细地址
    @Column(nullable = false, name = "address")
    private String address;

    // 还车地址
    @Column(nullable = false, name = "repay_address")
    private String repayAddress;

    // 电话
    @Column(nullable = false, length = 11, name = "tel")
    private String tel;
}
